import javafx.application.Platform;

import java.util.TimerTask;

/**
 * Class that keeps track of time, and the simulation in sync.
 */
public class Clock extends TimerTask {
    DataBase dataBase = DataBase.getInstance();
    //Times are measured in seconds
    public static int c_realTime=-1;
    public static int c_simTime= -1;
    private int multiplier;
    private GUIController GuiReference;
    private GameMaster GMReference;
    private DataBase DBReference;
    //GameMaster gameMaster;

    /**
     * Constructor
     */
    public Clock() {
        //c_realTime = -1;
        //c_simTime = -1;
        multiplier = 1;
        GuiReference = GUIController.GUIControllerReference;
        DBReference = DataBase.getInstance();
        GMReference = GameMaster.getInstance();
    }

    /**
     * Scheduler run function, Overrides a function from TimerTask
     */
    public void run() {
        c_realTime++;
        if (GuiReference.m_isPaused == false) {
                c_simTime++;
                //Update GUI
                GMReference.tick();
                dataBase.UpdateDatabase();
                if (GuiReference.m_readyForClock) {
                    Platform.runLater(() -> {
                        GuiReference.DisplaySim();
                    });
                }




        }

        // System.out.println(GetHumanReadableTime());

    }

    /*
    The Speed functions are used to set the sim run speed
     */


    /**
     * Getter
     * @return real time in seconds.
     */
    public int GetRealTime()
    {
        return c_realTime;
    }

    /**
     * Getter
     * @return sim time in seconds.
     */
    public int GetSimTime()
    {
        return c_simTime;
    }

    /**
     * Getter
     * @return a string that is in a normal timer format
     */
    public String GetHumanReadableTime()
    {
        int n = c_simTime;
        String ReadableTime;
        int seconds=0;
        int minutes=0;
        int hours=0;
        int days=0;

        days = n / (24*2600);
        hours = n/3600;
        n %= 3600;
        minutes = n / 60;
        n %= 60;
        seconds = n;

        /* Developer Note:
            The string can be made to output 0 values as 00 like a normal clock.
            If this is what is desired please contact Bradley Washburn to have him
            change string.
            Ex: instead of 0:0:5:20, it can be:     00:00:05:20
         */

        ReadableTime = days+":"+ hours+ ":"+minutes+":"+seconds;
        return (ReadableTime);
    }

    /**
     * Shutdown function to make sure clock shuts down when the GUI does. Called from GUI stop function.
     */
    public static void shutdown () {
        System.exit(0);
    }

}

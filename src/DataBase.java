import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;



/**
 * Class that holds all sim objects that are displayed.
 */
public class DataBase
{
    private static DataBase dataBase;

    public ArrayList<Predator> predatorList;
    public ArrayList <Plant> plantList;
    public ArrayList <Grazer> grazerList;
    public ArrayList <Obstacle> obstacleList;
    public ArrayList<Plant> newPlantsList;
    public ArrayList <Animal> graveyard;
    private ArrayList<LocationSquare> locationBoard;
    public ArrayList<Predator> unbornPredators;

    public int PredCount = 0;
    public int GrazeCount = 0;
    public int PlantCount = 0;
    public int obstacleCount=0;

    String FilePath;
    Path path;
    private DataBase()
    {
        //Lists to hold objects
        this.predatorList = new ArrayList<Predator>();
        this.plantList= new ArrayList<Plant>();
        this.grazerList=new ArrayList<Grazer>();
        this.obstacleList = new ArrayList<Obstacle>();
        this.graveyard = new ArrayList<Animal>();
        this.locationBoard = new ArrayList<LocationSquare>();
        this.newPlantsList= new ArrayList<Plant>();
        this.unbornPredators=new ArrayList<Predator>();

        //Create the directory to hold the print files from the create report
        String timestamp = new java.util.Date().toString();
        timestamp= timestamp.replace(":", "-");
        FilePath="."+File.separator+"Report "+timestamp;

        path = Paths.get(FilePath);
        try{
            Files.createDirectories(path);
        }
        catch(IOException e)
        {
            System.out.println("BAD!!!!!");
        }

    }

    /**
     * Data Base is implemented as a singleton so it can not be recreated.
     * @return  Returns an instance of the DataBase
     */
    public static DataBase  getInstance()
    {
        if (dataBase == null)
            dataBase = new DataBase();
        return dataBase;
    }

    public void addGrazer(Grazer grazer)
    {
        grazerList.add(grazer);
    }

    public void addPredator(Predator pred)
    {
        predatorList.add(pred);
    }

    public void addPredatorUnborn(Predator pred)
    {
        unbornPredators.add(pred);
    }

    public ArrayList<Predator> getV_pedatorList() {
        return predatorList;
    }

    public ArrayList<Plant> getV_plantList() {
        return plantList;
    }

    public ArrayList<Grazer> getV_grazerList() {
        return grazerList;
    }

    public ArrayList<Obstacle> getV_obstacleList() {
        return obstacleList;
    }

    public ArrayList<Predator> getV_unbornPredators()
    {
        return unbornPredators;
    }

    /**
     * this method iterates through the list of animals and plants,
     * finds the dead ones, removes all references to that animal from other animals,
     * then adds it the a graveyard list;
     *
     * @author d'Andre clifton
     */
    public void UpdateDatabase(){
        try{
            for(Predator preds : predatorList)
            {
                preds.visible_food.removeIf(f -> f.Dead == true);       //TODO: Throws an error sometiomes, investigate this
            }
        }
        catch (Exception e)
        {
            System.out.println("Access Issue");
        }


        predatorList.removeIf(p -> p.Dead == true);

        grazerList.removeIf(g -> g.Dead == true);
    }

    /**
     * Writes a reprot of the current state of a sim to a txt file
     * @param clock
     * @author Bradley Washburn
     */
    public void CreateReportDoc(Clock clock) {
        String simTimeStamp = (clock.GetHumanReadableTime()+".txt");
        simTimeStamp=simTimeStamp.replace(":", "-");
        File file = new File(path.toString(), simTimeStamp);

        double grazerEnergyAverage =0;
        double PredEnergyAverage=0;
        double PlantAverageDiameter=0;

        for(Grazer grazer:grazerList)
        {
            grazerEnergyAverage=grazerEnergyAverage+grazer.energy_level;
        }
        grazerEnergyAverage=grazerEnergyAverage/grazerList.size();

        for(Predator pred:predatorList)
        {
            PredEnergyAverage=PredEnergyAverage+pred.energy_level;
        }
        PredEnergyAverage=PredEnergyAverage/predatorList.size();

        for(Plant plant:plantList)
        {
            PlantAverageDiameter=PlantAverageDiameter+plant.getP_diameter();
        }
        PlantAverageDiameter=PlantAverageDiameter/plantList.size();

        try {
            File myObj = new File(file.toString());
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred creating the file.");
            e.printStackTrace();
        }

        try{
            FileWriter myWriter = new FileWriter(file);
            myWriter.write("This is an output file generated at the time of request containing all statuses of the Predators and Grazers in this simulation: \n\n");


            myWriter.write("Number of Grazers:  "+grazerList.size()+"\n");
            myWriter.write("Average Energy Level of all Grazers:    "+grazerEnergyAverage+"\n\n");

            myWriter.write("Number of Predators:    "+predatorList.size()+"\n");
            myWriter.write("Average Energy level of all Predators:  "+PredEnergyAverage+"\n\n");

            myWriter.write("Number of Plants:   "+plantList.size()+"\n");
            myWriter.write("Average Plant Diameter:  "+PlantAverageDiameter+"\n");



            myWriter.write("Predator Statuses: \n");
            for (Predator pred:predatorList)
            {
                myWriter.write("    Animal ID"+pred.UID+":"+"\n");
                myWriter.write("        Energy Level: "+pred.energy_level+"\n");
                myWriter.write("        Pregnant: "+pred.inGestation+"\n");
                myWriter.write("        XPOS: "+pred.getX_pos()+"\n");
                myWriter.write("        YPOS: "+pred.getY_pos()+"\n");
            }

            myWriter.write("\n\n\nGrazer Statuses: \n");
            for(Grazer graz:grazerList)
            {
                myWriter.write("    Animal ID"+graz.UID+":"+"\n");
                myWriter.write("        Energy Level: "+graz.energy_level+"\n");
                myWriter.write("        XPOS: "+graz.getX_pos()+"\n");
                myWriter.write("        YPOS: "+graz.getY_pos()+"\n");
            }
            myWriter.write("\n\n\nPlant Statuses: \n");
            for(Plant plant:plantList)
            {
                myWriter.write("    Plant ID"+plant.getP_id()+"\n");
                myWriter.write("        Current Diameter: "+plant.getP_diameter()+"\n");
                myWriter.write("        XPOS: "+plant.getX_pos()+"\n");
                myWriter.write("        YPOS: "+plant.getY_pos()+"\n");
            }

            myWriter.write("The Graveyard contains all animals that have died durring the sim");
            myWriter.write("Animal GraveYard: \n");
            for(Animal animal:graveyard)
            {
                myWriter.write("    "+animal.UID+"is dead");
            }
        myWriter.close();
        }
        catch(IOException e)
        {
            System.out.println("An error occurred while writting to the file.");
            e.printStackTrace();
        }
        System.out.println(file.toString());
    }

    /**
     * Used to transition the predator from the unborn to the born list
     * @param ID Takes an animal ID
     * @return  Returns a list of preds to be removed from the parents personal unborn list
     * @author Bradley Washburn
     */
    public ArrayList<Predator> PredatortoAdult(int ID)
    {
        ArrayList<Predator> toBeRemoved = new ArrayList<Predator>();
        for (Predator pred :unbornPredators)
        {
            if(ID==pred.UID)
            {
                predatorList.add(pred);
                toBeRemoved.add(pred);
            }
        }
        return toBeRemoved;
    }

}

import java.util.ArrayList;

public class LocationSquare
{
    /**
     * neighbors are not used as of yet. might come back to these
     */
    private int n_Neighbor;
    private int s_Neighbor;
    private int e_Neighbor;
    private int w_Neighbor;
    private int rowCount, colCount;
    private ArrayList<Character> flags = new ArrayList<Character>(10);

    /**
     *
     * @param col column number
     * @param row Row number
     */
    public LocationSquare(int col, int row)
    {
        this.colCount = col;
        this.rowCount = row;
        this.n_Neighbor = rowCount - 1;
        this.s_Neighbor = rowCount + 1;
        this.e_Neighbor = colCount + 1;
        this.w_Neighbor = colCount - 1;

    }




    public int getN_Neighbor() { return n_Neighbor;}

    public int getS_Neighbor() {
        return s_Neighbor;
    }

    public int getE_Neighbor() {
        return e_Neighbor;
    }

    public int getW_Neighbor() {
        return w_Neighbor;
    }

    public boolean appendFlag(char flag)
    {
        if(!(flags.contains(flag))) // if flag not already in flags
        {
            flags.add(flag);
            return true;
        }

        else{return false;}
    }

    public ArrayList<Character> getFlags() {
        return flags;
    }
}
